class AbstractCondtion(object):

    def evaluate(self, element):
        raise NotImplemented


class TextCondition(object):

    def __init__(self, text):
        self.text = text

    def evaluate(self, element):
        assert element.text == self.text


text = TextCondition