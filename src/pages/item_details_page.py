from src.pages.cart_page import CartPage


class ItemDetailsPage(object):

    def __init__(self, browser):
        self.browser = browser

    def add_to_cart(self):
        self.browser.element("input#add-to-cart-button").click()

    def open_cart(self):
        self.browser.element("a#nav-cart").click()
        return CartPage(self.browser)
