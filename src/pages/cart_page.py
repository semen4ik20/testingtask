class CartPage(object):

    def __init__(self, browser):
        self.browser = browser

    @property
    def item(self):
        return self.browser.element("span.a-truncate-cut")