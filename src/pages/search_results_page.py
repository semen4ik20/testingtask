from time import sleep


class SearchResultsPage(object):

    def __init__(self, browser):
        self.browser = browser

    def click_item(self, index):
        item_link = self.browser.element(f"div[data-cel-widget='search_result_{index}'] h2 a")
        selected_item_text = item_link.text
        item_link.click()
        return selected_item_text

    def get_item_text(self, index):
        try:
            element = self.browser.element(f"div[data-cel-widget='search_result_{index}'] h2 a")
            self.browser.scroll_to(element)
            return element.text
        except:
            return None

    def get_item_rating(self, index):
        try:
            return self.browser.element(f"div[data-cel-widget='search_result_{index}'] i.a-icon-star-small span").attr(
                'innerHTML')
        except Exception as e:
            return None
