from time import sleep

from src.pages.search_results_page import SearchResultsPage


class MainPage(object):

    def __init__(self, browser, url):
        self.url = url
        self.browser = browser

    def open(self):
        self.browser.get(self.url)
        return self

    def search(self, text):
        self.browser.element("#twotabsearchtextbox").set_value(text)
        self.browser.element("input[type='submit']").click()
        sleep(1)
        return SearchResultsPage(self.browser)


