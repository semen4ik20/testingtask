from src.pages.item_details_page import ItemDetailsPage
from src.pages.main_page import MainPage


class Application(object):

    def __init__(self, browser, config):
        self.config = config
        self.browser = browser

    @property
    def main_page(self):
        return MainPage(self.browser, self.config['url'])

    @property
    def item_details(self):
        return ItemDetailsPage(self.browser)