import csv


class Storage(object):

    def __init__(self):
        pass

    def save(self, data):
        raise NotImplemented()


class CsvStorage(Storage):

    def __init__(self, name = 'items.csv'):
        super().__init__()
        self.header = ['name', 'rating']
        self.name = name

    def save(self, data):
        with open(self.name, 'w') as file:
            # 2. Create a CSV writer
            writer = csv.writer(file)
            # 3. Write data to the file
            writer.writerow(self.header)
            writer.writerows(data)
