from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


class BrowserFactory(object):

    def __init__(self):
        pass

    @staticmethod
    def get_browser(name):
        if name == "chrome":
            service = Service(executable_path=ChromeDriverManager().install())
            return webdriver.Chrome(service=service)

        from webdriver_manager.firefox import GeckoDriverManager
        return webdriver.Firefox(executable_path=GeckoDriverManager().install())
