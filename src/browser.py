from selenium.webdriver.common.by import By


class Browser(object):

    def __init__(self, driver):
        self.driver = driver

    def get(self, url):
        self.driver.get(url)

    def element(self, selector, by=By.CSS_SELECTOR):
        return Element(self.driver.find_element(by=by, value=selector))

    def all(self, selector, by=By.CSS_SELECTOR):
        return self.driver.find_elements(by=by, value=selector)

    def scroll_to(self, element):
        self.driver.execute_script("arguments[0].scrollIntoView();", element.element)

    def close(self):
        self.driver.quit()


class Element(object):

    def __init__(self, element):
        self.element = element

    def should_have(self, condition):
        condition.evaluate(self.element)

    def click(self):
        self.element.click()

    def set_value(self, text):
        self.element.send_keys(text)

    @property
    def text(self):
        return self.element.text

    def attr(self, name):
        return self.element.get_attribute(name)
