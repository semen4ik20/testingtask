from time import sleep

import pytest

from src.browser import Browser
from src.factory import BrowserFactory
from src.pages.application import Application
from src.pages.storage import CsvStorage


@pytest.fixture(scope="session")
def app():
    config = {
        "url": "https://amazon.com",
        "browser": "chrome"
    }

    browser = Browser(BrowserFactory.get_browser(config['browser']))
    yield Application(browser, config)
    browser.close()


def test_can_add_item_to_cart(app):
    main_page = app.main_page.open()
    search_results = main_page.search("software testing")
    selected_item_text = search_results.click_first_item()
    app.item_details.add_to_cart()
    cart_page = app.item_details.open_cart()
    cart_item_text = cart_page.item.text
    assert selected_item_text == cart_item_text


def test_scrape_data(app):
    csv = CsvStorage()
    main_page = app.main_page.open()
    search_results = main_page.search("software testing")

    data = []
    for index in range(1, 20):
        item_text = search_results.get_item_text(index)
        if not item_text:
            continue

        item_rating = search_results.get_item_rating(index)
        print(f"{index} {item_text} {item_rating}")
        data.append([item_text, item_rating])

    csv.save(data)

    assert get_average_rating(data) > 3
    assert len(find_title(data, "Modern CMake")) == 0


def get_average_rating(data):
    result = []
    for item in data:
        rating = item[1]
        if rating:
            value = float(rating.split(" ")[0])
            result.append(value)
    return sum(result) / len(result)


def find_title(data, title):
    titles = map(lambda x: x[0], data)
    result = list(filter(lambda t: title in t, titles))
    return result
